Errors = {
	// Local (client-only) collection
	collection: new Mongo.Collection(null),

	throwMessage: function(message) {
		Errors.collection.insert({message: message, seen: false, type: "success"});
	},

	throwError: function(message) {
		Errors.collection.insert({message: message, seen: false, type: "danger"});
	},

	clearErrors: function() {
		Errors.collection.remove({seen: true});
	}
}