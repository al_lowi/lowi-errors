Package.describe({
  name: 'lowi-errors',
  summary: ' Error and message notifications based on the book discover meteor',
  version: '1.0.0',
  git: 'https://al_lowi@bitbucket.org/al_lowi/lowi-errors.git'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0');
  api.use(['minimongo', 'mongo-livedata', 'templating'], 'client');
  api.use("mquandalle:jade")
  api.addFiles(['errors.js', 'errors_list.jade', 'errors_list.js'], 'client');
  if (api.export) 
	api.export('Errors');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('errors');
  api.addFiles('errors-tests.js');
});
