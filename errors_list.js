Template.appErrors.helpers({
    errors: function() {
        return Errors.collection.find();
    }
});

Template.appError.rendered = function() {
   var error = this.data;
    Meteor.defer(function() {
        Errors.collection.update(error._id, {$set: {seen:true}});
        Meteor.setTimeout(function () {
            Errors.collection.remove(error._id);
        }, 3000);
    });
};